<?php

namespace Drupal\sample_display;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class SampleDisplay.
 *
 * @package Drupal\sample_display
 */
class SampleDisplay implements EventSubscriberInterface {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * Name of the cookie this service will manage.
   *
   * @var string
   */
  protected $cookieName = 'sample_display_cookie';

  /**
   * The cookie value that will be set during the respond event.
   *
   * @var mixed
   */
  protected $newCookieValue;

  /**
   * Whether or not the cookie should be updated during the response.
   *
   * @var bool
   */
  protected $shouldUpdateCookie = FALSE;

  /**
   * Whether or not the cookie should be deleted during the response.
   *
   * @var bool
   */
  protected $shouldDeleteCookie = FALSE;

  /**
   * SampleDisplay constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack service.
   */
  public function __construct(RequestStack $request_stack) {
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * Get this cookie's name.
   *
   * @return string
   */
  public function getCookieName() {
    return $this->cookieName;
  }

  /**
   * Get the cookie's value.
   *
   * @return mixed
   *   Cookie value.
   */
  public function getCookieValue() {
    // If we're mid-request and setting a new cookie value, return that new
    // value. This allows other parts of the system access to the most recent
    // cookie value.
    if (!empty($this->newCookieValue)) {
      return $this->newCookieValue;
    }

    return $this->request->cookies->get($this->getCookieName());
  }

  /**
   * Set the cookie's new value.
   *
   * @param mixed $value
   */
  public function setCookieValue($value) {
    $this->shouldUpdateCookie = TRUE;
    $this->newCookieValue = $value;
  }

  /**
   * Get the cookie's expire.
   *
   * @return mixed
   *   Cookie expire.
   */
  public function getCookieExpire() {
    if (!empty($this->newCookieExpire)) {
      return $this->newCookieExpire;
    }

    return $this->request->cookies->get($this->getCookieName());
  }

  /**
   * Set the cookie's new expire.
   *
   * @param mixed $expire
   */
  public function setCookieExpire($expire) {
    $this->shouldUpdateCookie = TRUE;
    $this->newCookieExpire = $expire;
  }

  /**
   * Get the cookie's path.
   *
   * @return mixed
   *   Cookie path.
   */
  public function getCookiePath() {
    if (!empty($this->newCookiePath)) {
      return $this->newCookiePath;
    }

    return $this->request->cookies->get($this->getCookieName());
  }

  /**
   * Set the cookie's new path.
   *
   * @param mixed $path
   */
  public function setCookiePath($path) {
    $this->shouldUpdateCookie = TRUE;
    $this->newCookiePath = $path;
  }

  /**
   * Whether or not the cookie should be updated during the response.
   *
   * @return bool
   */
  public function getShouldUpdateCookie() {
    return $this->shouldUpdateCookie;
  }

  /**
   * Whether or not the cookie should be deleted during the response.
   *
   * @return bool
   */
  public function getShouldDeleteCookie() {
    return $this->shouldDeleteCookie;
  }

  /**
   * Set whether or not the cookie should be deleted during the response.
   *
   * @param bool $delete_cookie
   *   Whether or not to delete the cookie during the response.
   */
  public function setShouldDeleteCookie($delete_cookie = TRUE) {
    $this->shouldDeleteCookie = (bool) $delete_cookie;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => 'onResponse',
    ];
  }

  /**
   * React to the symfony kernel response event by managing visitor cookies.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();

    if ($this->getShouldUpdateCookie()) {
      $newSampleDisplay = new Cookie($this->getCookieName(), $this->getCookieValue() , $this->getCookieExpire() , $this->getCookiePath(), NULL, FALSE);
      $response->headers->setCookie($newSampleDisplay);
    }

    // The "should delete" needs to happen after "should update", or we could
    // find ourselves in a situation where we are unable to delete the cookie
    // because another part of the system is trying to update its value.
    if ($this->getShouldDeleteCookie()) {
      $response->headers->clearCookie($this->getCookieName());
    }
  }

}
